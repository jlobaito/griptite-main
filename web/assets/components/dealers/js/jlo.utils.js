
//console.dir(sto);
jlo.util = {

	"log" : function (x,y) {
		if (window.console && console.log)
			console.log( x + ' - ' + y);
	},
	"dump" : function(x,y) {
		if (window.console && console.dir){
			console.log(x);
			console.dir(y);
		}
	},
	"queryParam" : function( name )  {
		name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
		var regexS = "[\\?&]"+name+"=([^&#]*)";
		var regex = new RegExp( regexS );
		var results = regex.exec( window.location.href );
		if( results === null )
			return "";
		else
			return decodeURIComponent(results[1].replace(/\+/g, " "));
	},
	"xhr" : function(o){

		var options = {
			type: "GET",
			dataType: "json",
			show_loading: true
		};
		$.extend(options, o);
		//jlo.util.dump("options",options);
		if(options.show_loading === true){
			$("#loading-row").show();
		}

		var promise = $.ajax(options);
		return promise;
	},
	"template" : function(data){
		//console.dir(data);
		var template = $(data.template).html(); // need to use .html instead of .text to get tags
		template = u.unescape(template); // needed to remove encoding on the <%= and %> tags

		jlo.util.dump("template data",data);
		var compiled = u.template(template, data.data);
	//	console.log(compiled);
		if(data.append === true ){
			compiled = $(data.target).html() + compiled; // removing template from DOM so that ids do not conflict
		}
		if(data.prepend === true ){
			compiled = compiled + $(data.target).html(); // removing template from DOM so that ids do not conflict
		}
		//console.log(compiled);
		$(data.target).html(compiled);
		if(data.remove === true){
			$(data.template).remove(); // removing template from DOM so that ids do not conflict
		}
	}
};

$.fn.serializeObject = function()
{
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name] !== undefined) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};
