jlo.app.Dealers = function() {
    var self = this;

    this.countiesTemplate = function() {
        var data = {
            'counties': jlo.data.counties[jlo.data.current_state]
        };
        jlo.util.dump("counties", data);
        var settings = {
            template: ".county-list-template",
            target: ".county-list-target",
            data: data
        };
       jlo.util.template(settings);
       $(".county-list .checked").attr("checked", "checked");
    };
};
jlo.dealers = new jlo.app.Dealers();

jlo.app.activateDealers = function() {
	var self = this;

	this.dealerState = function() {
		$(".dealer-state").unbind('change').change(function() {
			var $e = $(this);
			if($e.val().length == 2){
				jlo.data.current_state = $e.val();
				jlo.dealers.countiesTemplate();
			}
		});
	};

	this.dealerCountry = function() {
		$(".dealer-country").unbind('change').change(function() {
			var $e = $(this);
			jlo.util.log($e.val(), "country");
			if($e.val().length == 2){
				jlo.data.current_country = $e.val();
				if(jlo.data.current_country == 'US'){
					$(".hidden-us").hide();
					$(".hidden-us select").val('');
					$(".hidden-ca").show();
				} else {
					$(".hidden-us").show();
					$(".hidden-ca").hide();
					$(".hidden-ca select").val('');
				}
			}
		});
	};

	this.salesRepFlag = function() {
		$(".sales-rep-flag").unbind('click').click(function() {
			var $e = $(this);

			jlo.util.log($e.val().length + " " + $e.val());
			if($e.is(":checked")){
				jlo.util.log(":checked show");
				$(".county-list").removeClass("hide");
			} else {
				jlo.util.log(":checked hide");
				$(".county-list").addClass("hide");
			}

		});
	};

};

jlo.activateDealers = new jlo.app.activateDealers();

(function($) {
    //jlo.dealers.getDealers();
    jlo.activateDealers.dealerState();
    jlo.activateDealers.dealerCountry();
    //jlo.activateDealers.browserTrigger();
})(jQuery);