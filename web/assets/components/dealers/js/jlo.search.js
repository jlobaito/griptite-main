
jlo.app.Search = function() {
    var self = this;
    this.doSearch = function(data) {
        //var data = {};
       // data.action = "search";
        data.action = (jlo.data.selected_country == "US") ? "search" : "search-ca";
        var options = {
            'method': "GET",
            'url': '/core/components/dealers/controllers/dealers.php',
            'data': data
        };
        var remote = jlo.util.xhr(options);
        $.when(remote).then(function(data) {
			jlo.data.results = data.data;
			jlo.util.dump("results", jlo.data.results);
			
			jlo.search.resultsTemplate();
            return true;
        });
        self.jumpToResults();
    };

    this.resultsTemplate = function() {
        var data = {
            'retailers': jlo.data.results.retailers,
            'message': jlo.data.results.message,
            'headquarters': jlo.data.results.headquarters
        };
        jlo.util.dump("results", data);
        var settings = {
            template: ".results-list-template",
            target: ".results-list-target",
            data: data
        };
       jlo.util.template(settings);
       $(".county-list .checked").attr("checked", "checked");
    };

    this.autoSearch = function() {
        var zip_code = jlo.util.queryParam("zip_code");
        if(zip_code !== ""){
            $(".zip_code").val(zip_code);
            var data = {zip_code: zip_code};
            self.doSearch(data);
        }
    };
    this.jumpToResults = function() {
        //$('body').scrollTo('.search-results');
        $('html, body').animate({scrollTop: $('.search-results').offset().top - 80}, 700);
    };
};
jlo.search = new jlo.app.Search();

jlo.app.activateSearch = function() {
	var self = this;

	this.submitSearch = function() {
		$(".submit-search").unbind('click').click(function() {
			var $e = $(this);
			var form = $($e.data("form")).serializeObject();
			jlo.search.doSearch(form);
		});
	};
    this.countrySelect = function() {
        $(".country-select").unbind('click').click(function() {
            var $e = $(this);
            jlo.data.selected_country = $e.data("country");
            $(".country-select").removeClass('active');
            $e.addClass('active');
        });
    };
	this.keypressEnter = function() {
		$("#search-location-form, #search-text-form").unbind('keypress').keypress(function(e) {
            var $e = $(this);
            if (e.keyCode === 13) {
                e.preventDefault();
                $(".submit-search", $e).click();
            }
		});
	};
};
jlo.activateSearch = new jlo.app.activateSearch();

(function($) {
    //jlo.search.getSearch();
    jlo.data.selected_country = "US";
    jlo.activateSearch.submitSearch();
    jlo.activateSearch.countrySelect();
    jlo.activateSearch.keypressEnter();
    jlo.search.autoSearch();
})(jQuery);