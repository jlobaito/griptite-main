<?php
/**
 * Get a list of ContactSubmissions
 *
 * @package contactsubmissions
 * @subpackage processors
 */
class ContactSubmissionGetListProcessor extends modObjectGetListProcessor {
    public $classKey = 'ContactSubmission';
    public $languageTopics = array('contactsubmissions:default');
    public $defaultSortField = 'createdon';
    public $defaultSortDirection = 'ASC';
    public $objectType = 'contactsubmissions.contactsubmission';

    public function prepareQueryBeforeCount(xPDOQuery $c) {
        $query = $this->getProperty('query');
        if (!empty($query)) {
            $c->where(array(
                'name:LIKE' => '%'.$query.'%',
                'OR:email:LIKE'  => '%'.$query.'%',
            ));
        }
        return $c;
    }
}
return 'ContactSubmissionGetListProcessor';