<?php

class Util {
	
	private $data;
	public $alert;

	function __construct() {
		//$this->buildAlerts();
	}

	public function stateSelect($display='abbr',$name='state',$id='',$class='',$active=''){
	    $states_arr = array('AL'=>"Alabama",'AK'=>"Alaska",'AZ'=>"Arizona",'AR'=>"Arkansas",'CA'=>"California",'CO'=>"Colorado",'CT'=>"Connecticut",'DE'=>"Delaware",'DC'=>"District Of Columbia",'FL'=>"Florida",'GA'=>"Georgia",'HI'=>"Hawaii", 'IA'=>"Iowa", 'ID'=>"Idaho",'IL'=>"Illinois", 'IN'=>"Indiana",  'KS'=>"Kansas",'KY'=>"Kentucky",'LA'=>"Louisiana",'ME'=>"Maine",'MD'=>"Maryland", 'MA'=>"Massachusetts",'MI'=>"Michigan",'MN'=>"Minnesota",'MS'=>"Mississippi",'MO'=>"Missouri",'MT'=>"Montana",'NE'=>"Nebraska",'NV'=>"Nevada",'NH'=>"New Hampshire",'NJ'=>"New Jersey",'NM'=>"New Mexico",'NY'=>"New York",'NC'=>"North Carolina",'ND'=>"North Dakota",'OH'=>"Ohio",'OK'=>"Oklahoma", 'OR'=>"Oregon",'PA'=>"Pennsylvania",'RI'=>"Rhode Island",'SC'=>"South Carolina",'SD'=>"South Dakota",'TN'=>"Tennessee",'TX'=>"Texas",'UT'=>"Utah",'VT'=>"Vermont",'VA'=>"Virginia",'WA'=>"Washington",'WV'=>"West Virginia",'WI'=>"Wisconsin",'WY'=>"Wyoming");
	    $string="<select name='$name' id='$id' class='$class'>";
		$string.='<option value="">Select State</option>';
		foreach($states_arr as $k => $v){
			$s = ($active == $k)? ' selected="selected"' : '';

			if($display=='abbr'){
				$string .= '<option value="'.$k.'"'.$s.'>'.$k.'</option>'."\n";     
			}else{
				$string .= '<option value="'.$k.'"'.$s.'>'.$v.'</option>'."\n";     
			}
	    }
	    $string .= '</select>';
		return $string;
	}

	public function latLngRange($lat, $long, $distance = 50){
	
		$earthRadius = 3958.75; //in miles
		$long_rad = deg2rad($long);
		$lat_rad = deg2rad($lat);

		$north 	= deg2rad(360); 
		$south 	= deg2rad(180);
		$east 	= deg2rad(90);
		$west 	= deg2rad(270);

		$c = $distance / $earthRadius;

		$ranges = array();

		$ranges["lat_min"] = rad2deg(asin( sin ($lat_rad) * cos($c) + cos($lat_rad) * sin($c) * cos($north)));
		$ranges["lat_max"] = rad2deg(asin( sin ($lat_rad) * cos($c) + cos($lat_rad) * sin($c) * cos($south)));

		$a_north = sin($c) * sin($west);
		$a_south = sin($c) * sin($east);

		$b_north = cos($lat_rad) * cos($c) - sin($lat_rad)* cos($c)* cos($west);
		$b_south = cos($lat_rad) * cos($c) - sin($lat_rad)* cos($c)* cos($east);

		if ($b_north == 0){
			$ranges["lng_min"] = rad2deg($long_rad);
		} else {
			$ranges["lng_min"] = rad2deg($long_rad + atan($a_north/$b_north));
		}

		if ($b_south == 0){
			$ranges["lng_max"] = rad2deg($long_rad);
		} else {
			$ranges["lng_max"] = rad2deg($long_rad + atan($a_south/$b_south));
		}

		return $ranges;
	}

	public function randomFloat ($min,$max) {
		return ($min+lcg_value()*(abs($max-$min)));
	}

	public function distanceInFeet($lat1, $lng1, $lat2, $lng2, $unit = "M") {
		//echo $lat1 . " - " . $lng1 . " - " . $lat2 . " - " . $lng2 . " - " . $unit;
		$theta = $lng1 - $lng2;
		error_log("theta " . $theta);

		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		//error_log("dist " . $dist);
		$dist = acos($dist);
		//error_log("dist " . $dist);
		$dist = rad2deg($dist);
		//error_log("dist " . $dist);
		$miles = $dist * 60 * 1.1515;
		//error_log("miles " . $miles);
		$unit = strtoupper($unit);

		if ($unit == "K") {
			$result["distance"] = ($miles * 1.609344);
		} else if ($unit == "N") {
			$result["distance"] = ($miles * 0.8684);
		} else if ($unit == "F"){
			$result["distance"] = $miles * 5281;
		} else {
			$result["distance"] = number_format($miles,2);
		}

		$bearingDeg = (rad2deg(atan2(sin(deg2rad($lng2) - deg2rad($lng1)) * 
			cos(deg2rad($lat2)), cos(deg2rad($lat1)) * sin(deg2rad($lat2)) - 
			sin(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($lng2) - deg2rad($lng1)))) + 360) % 360;

		$result["bearing"] = $this->bearingLabel($bearingDeg);;
		return $result;
	}

	public function bearingLabel($degrees) {

		$labels = array ("N","NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S","SSW","SW", "WSW", "W", "WNW", "NW", "NNW");

		$label = $labels[ fmod((($degrees + 11) / 22.5),16) ];
		return $label;

	} 

	public function parse_csv_file($csvfile) {
	    $csv = Array();
	    $rowcount = 0;
	    if (($handle = fopen($csvfile, "r")) !== FALSE) {
	        $max_line_length = defined('MAX_LINE_LENGTH') ? MAX_LINE_LENGTH : 10000;
	        $header = fgetcsv($handle, $max_line_length);
	        $header_colcount = count($header);
	        while (($row = fgetcsv($handle, $max_line_length)) !== FALSE) {
	            $row_colcount = count($row);
	            if ($row_colcount == $header_colcount) {
	                $entry = array_combine($header, $row);
	                $csv[$row[0]] = $entry;
	            }
	            else {
	                error_log("csvreader: Invalid number of columns at line " . ($rowcount + 2) . " (row " . ($rowcount + 1) . "). Expected=$header_colcount Got=$row_colcount");
	                return null;
	            }
	            $rowcount++;
	        }
	        //echo "Totally $rowcount rows found\n";
	        fclose($handle);
	    }
	    else {
	        error_log("csvreader: Could not read CSV \"$csvfile\"");
	        return null;
	    }
	    return $csv;
	}

	public function buildAlerts(){
		$error = $this->parse_csv_file($_SERVER['DOCUMENT_ROOT'].'/sb-connect/resources/data/error.csv');
		$info = $this->parse_csv_file($_SERVER['DOCUMENT_ROOT'].'/sb-connect/resources/data/info.csv');
		$success = $this->parse_csv_file($_SERVER['DOCUMENT_ROOT'].'/sb-connect/resources/data/success.csv');

		$this->alert = $error + $info + $success;


	}

	public function cleanInput($input) {
	  $search = array(
	    '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
	    '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
	    '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
	    '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
	  );
	    $output = preg_replace($search, '', $input);
	    return $output;
	}

	public function sanitize($input) {
	    if (is_array($input)) {
	        foreach($input as $var=>$val) {
	            $output[$var] = sanitize($val);
	        }
	    }
	    else {
	        if (get_magic_quotes_gpc()) {
	            $input = stripslashes($input);
	        }
	        $input  = $this->cleanInput($input);
	        $output = mysql_real_escape_string($input);
	    }
	    return $output;
	} 


	// used for dynamically setting column names and table names
	public function sanitizeField($field) {
		$clean = preg_replace('/[^a-zA-Z0-9_ -]/','',$field);
		return $clean;
	}
	
	// used for dynamically setting column names and table names
	public function sanitizeValue($value) {
		//$value = preg_replace('/((\%3C)|<)((\%2F)|\/)*[a-z0-9\%]+((\%3E)|>)/ix','',$value); //css simple
		$value = preg_replace('/((\%27)|(\'))union/ix','',$value); //sql injection with union
		$value = preg_replace('/\w*((\%27)|(\'))((\%6F)|o|(\%4F))((\%72)|r|(\%52))/ix','',$value); //sql injection typical
		//$value = preg_replace('/((\%3D)|(=))[^\n]*((\%27)|(\')|(\-\-)|(\%3B)|(;))/i','',$value); //sql injection meta characters
		//$value = htmlentities($value);
		return $value;
	}

	public function mapFields($data, $keys) {
		$tmp = array();
		$data = (array) $data;
		foreach ($keys as $key => $val) {
			if (isset($data[$key]))
				$tmp[$key] = $this->sanitizeValue($data[$key]);
		}
		//$this->pts($tmp);
		return $tmp;
	}

	public function prepareInsert($data) {
		$tmp = array();
		foreach ($data as $d) {
				$question_marks[] = "?";
				$insert_data[] = $d;
		}
		$tmp["question_marks"] = $question_marks;
		$tmp["data"] = $insert_data;
		return $tmp;
	}

	public function prepareUpdate($data, $keys) {

		// constructing set clause		
		$tmp = array();

		$tmp["set"] = " ";
		foreach ($keys as $key => $val) {
			if (isset($data[$key])) {
				$tmp["set"] .=  " " . $key . "=?,";
				$tmp["fields"][$key] = $this->sanitizeField($data[$key]);
				$tmp["values"][] = $this->sanitizeValue($data[$key]);
			}
		}

		$tmp["set"] = trim($tmp["set"], ",");
		return $tmp;
	}

	public function  prepareWhere($data, $where_field = null, $key_id = null){
		$tmp = array();
		// constructing where clause
		$tmp["statement"] = " WHERE ";
		if ($where_field == null) {
			if($key_id == null){
				$tmp["statement"] = null;
			} else {
				$tmp["statement"] .= "$key_id = ?";
				$tmp["values"][] = $this->sanitizeValue($data['update_id']);
			}
		} else {
			if(is_array($where_field)){
				foreach($where_field as $w){
					$tmp["statement"] .= $w.' = ? AND ';
					$tmp["values"][] = $this->sanitizeValue($data[$w]);
				}
				 $tmp["statement"] = rtrim($tmp["statement"], 'AND ');
			} else {
				$tmp["statement"] .= $where_field.' = ?';
				$tmp["values"][] = $this->sanitizeValue($data[$where_field]);
			}
		}
		return $tmp;
	}

	public function pts($data, $message = null){
		echo "<h1>$message</h1><pre>";
		print_r($data);
		echo "</pre>";
	}


	public function getCountiesByState($data) {
		global $modx;
		$table = "geo_counties";
		//$result = $modx->select('id, address, city, state, zip',$table,'longitude = NULL','',5);
		$sql = "SELECT county FROM $table WHERE state = '" . $data->state . "' ORDER BY county ASC";
		//echo $sql;
		$result = $modx->query($sql);
		$objs = array();
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$obj = $row["county"];
			array_push($objs, $obj);
		}
		//print_r($objs);
		return $objs;

	}
	public function getCounties() {
		global $modx;
		$table = "geo_counties";
		//$result = $modx->select('id, address, city, state, zip',$table,'longitude = NULL','',5);
		$sql = "SELECT county, state FROM $table ORDER BY county ASC";
		//echo $sql;
		$result = $modx->query($sql);
		$objs = array();
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$objs[$row["state"]][] = $row["county"];
			//array_push($objs, $obj);
			//print_r($obj);
			//die();
		}
		//print_r($objs);
		return $objs;

	}

	static function getChildren($data = null){
		global $modx;
		$data["parent"] = (isset($data["parent"])) ? $data["parent"] : 2 ;

		$results = array();
		$sortBy = (isset($sortBy)) ? $sortBy : "pagetitle";
		$sortOrder = (isset($sortOrder)) ? $sortOrder : "ASC";

		$c = $modx->newQuery('modResource');
		$c->where($data);
		$c->sortby($sortBy, $sortOrder);

		$resources = $modx->getCollection( 'modResource', $c );

		$matches = array();

		foreach ( $resources as $resource ) {
			$result = array();
		    $result["id"] = $resource->id;
		    $result["pagetitle"] = $resource->pagetitle;
		    $result["alias"] = $resource->alias;
		    $result["longtitle"] = $resource->longtitle;
		    $result["parent"] = true;
			$results[] = $result;

			$data["parent"] = $resource->id;
			//print_r($data);

			$c = $modx->newQuery('modResource');
			$c->where($data);
			$c->sortby($sortBy, $sortOrder);

			$children = $modx->getCollection( 'modResource', $c );
			if(!empty($children)){
				foreach ( $children as $child ) {
					$result = array();
				    $result["id"] = $child->id;
				    $result["pagetitle"] = "--" . $child->pagetitle;
				    $result["alias"] = $child->alias;
				    $result["longtitle"] = "  --" . $child->longtitle;
					$results[] = $result;
				}
			}

		}

		return $results;
	}

	public function isValidEmail( $email ){
		return preg_match( "/^[_a-z0-9-]+(\.[_a-z0-9+-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i", $email );
	}

	public function linkify($text){
		$text = str_replace("/", "-", trim($text));
		$text = str_replace(" ", "-", $text);
		$text = str_replace("---", "-", $text);
		$text = strtolower(str_replace("--", "-", $text));
		return $text;
	}
	
}
?>