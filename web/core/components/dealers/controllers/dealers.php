<?php
error_reporting(E_ALL);
//ini_set('display_errors', 1);
//echo " hi ";
if(!isset($modx))
{
	//echo " modx config ";
    require_once $_SERVER['DOCUMENT_ROOT'].'/config.core.php';
    require_once MODX_CORE_PATH.'model/modx/modx.class.php';
    $modx = new modX();
    $modx->initialize('web');
  //  $modx->getService('error','error.modError');
}
include_once($_SERVER['DOCUMENT_ROOT'].'/core/components/dealers/models/Dealer.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/core/components/common/models/Util.php');
 
$u = new Util();

$controller_action = (isset($_GET["action"])) ? $_GET["action"] : $controller_action ;

$response["success"] = true;
	switch($controller_action){

		case 'sign-up' :
			$response["status"] = false;
			$data = (isset($_POST["email"])) ? $_POST : $_GET;
			
			if(isset($data['email'])){

					if(!Dealer::does_email_exist($data['email'])){
						$insert_id = Dealer::create_from_form_data($data);
						if(intval($insert_id) > -1){
							$user = new Dealer($insert_id);
							$_SESSION['user'] = serialize($user);
							
							$response["type"] = $user->getType();;
							
							$response["status"] = true;
						} 
					}else{
						$response["message"] = $u->alert[10006];;
					}
			}

		break;
		
		case 'update':
			$response["status"] = false;
			$data = (isset($_POST["save"])) ? $_POST : $_GET;

			if(isset($data['save'])){				
				//UPDATE THE USER!!!
				if(!empty($data["name"])){
					$tmp = explode(" ", $data["name"]);
					$data["fname"] = $tmp[0];
					$data["lname"] = $tmp[1];
				}
				if(Dealer::update_from_post($data)){
					$response["data"] = unserialize($_SESSION["user"]);
					$response["status"] = true;
					$response["message"] = $u->alert[20002];
				};
			}
		break;

		case 'search' :
			$response['status'] = false;
			$data = (isset($_POST["action"])) ? $_POST : $_GET;
			if(isset($data['action'])){
				$user = new Dealer();
				$results = $user->search($data);
				
				if(empty($results["retailers"])){
					$results["message"] = "We are sorry but no licensed Grip-Tite installers were found in this area. If you have any questions feel free to contact our corportate location below:";
				}
				
				$response['data'] = $results;
				$response['status'] = true;
			} else {
				//zip code was not set
				$response['data'] = null;
				$response['status'] = true;
				$results["message"] = "We are sorry but no licensed Grip-Tite installers were found in this area. If you have any questions feel free to contact our corportate location below:";
			}
		break;

		case 'search-ca' :
			$response['status'] = false;
			$data = (isset($_POST["action"])) ? $_POST : $_GET;
			if(isset($data['action'])){
				$user = new Dealer();
				$results = $user->searchZip($data);
				$count = 1;
				while(empty($results["retailers"]) && isset($results["range"]) && !isset($results["kill"]) && $results["range"] <= 100 && !empty($data["zip_code"])){
					$data["range"] = $results["range"] + 10;
					$results = $user->searchZip($data);
					$results["message"] = "We are sorry no results match your query, however the following is the closest retailer.";
					//$count++;
				}
				if(empty($results["retailers"])){
					$results["message"] = "We are sorry but no licensed Grip-Tite installers were found in this area. If you have any questions feel free to contact our corportate location below:";
				}
				$response['data'] = $results;
				$response['status'] = true;
			} else {
				//zip code was not set
				$response['data'] = null;
				$response['status'] = true;
				$results["message"] = "We are sorry no results match your query.";
			}
		break;

		case 'setGeo' :
			echo "in SetGeo";
			$u = new dealer();
			Dealer::getByEmptyGeo();

		break;
}

echo json_encode($response);
?>