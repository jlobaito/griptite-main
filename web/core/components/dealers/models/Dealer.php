<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once($_SERVER["DOCUMENT_ROOT"] .'/core/components/common/models/DataBase.php');

class Dealer extends DataBase{
	static $CLASS_TABLE = "dealers";
	
	var $dealer_id;
	var $content;
	var $company;
	var $headquarters_flag;
	var $title;
	var $email;
	var $address;
	var $address2;
	var $city;
	var $state;
	var $country;
	var $counties;
	var $zip_code;
	var $phone;
	var $tollfree;
	var $website;
	var $fax;
	var $longitude;
	var $latitude;
	var $created_at;
	var $updated_at;

	static $jsons = array("counties");
	static $remove = array("crypted_password", "salt","password_reset");
	
	function __construct($i_id=null,$i_obj=null){
		if ($i_id != null) {
			$sql = "SELECT * FROM " . Dealer::$CLASS_TABLE . " WHERE dealer_id = $i_id LIMIT 1";

			$pdo = $this->getPDO();
			$stmt = $pdo->prepare($sql);
			$result = $stmt->execute();

			if ($stmt->rowCount() > 0) {
				$results = $stmt->fetchAll(\PDO::FETCH_CLASS);
				foreach ($results[0] as $key => $val) {
					if(in_array($key, Dealer::$jsons)){
						$this->$key = json_decode($val);
					} else {
						$this->$key = $val;
					}
				}
				foreach(Dealer::$remove as $r){
					unset($this->$r);
				}
			}
		}
	}	
	
	static function getAll() {
		$objs = array();
		$sql = "SELECT dealer_id FROM " . Dealer::$CLASS_TABLE . " ORDER BY company ASC";
		$d = new Dealer();
		$pdo = $d->getPDO();
		$stmt = $pdo->prepare($sql);
		$result = $stmt->execute();

		if ($stmt->rowCount() > 0) {
			$results = $stmt->fetchAll(\PDO::FETCH_CLASS);
			foreach ($results as $r) {
				$objs[] = new Dealer($r->dealer_id);
			}
		}
		return $objs;
		
	}
	
	static function getCountries() {
		
		$sql = "SELECT country FROM " . Dealer::$CLASS_TABLE . " WHERE international_flag = 1 GROUP BY country ORDER BY country ASC";
		$objs = array();
		$d = new Dealer();
		$pdo = $d->getPDO();
		$stmt = $pdo->prepare($sql);
		$result = $stmt->execute();

		if ($stmt->rowCount() > 0) {
			$results = $stmt->fetchAll(\PDO::FETCH_CLASS);
			foreach ($results as $r) {
				$objs[] = $row->country;
			}
		}
		//print_r($objs);
		return $objs;
		
	}

	public function getType(){
		return Dealer::$user_types[$this->user_type];
	}
	
	/** 
	* create from form data
	* @param $i_obj obj data for record insert
	* @return a id of inserted record or query message
	*/
	static function create($i_obj) {
		$d = new Dealer();
		$fields = array();
		error_reporting(E_ALL);

		$fields["company"] = "New Company";
		$fields['created_at'] = date("Y-m-d H:i:s"); // set created timestamp

		$insert = $d->prepareInsert($fields);

		//$table = $d->table;
		$sql = "INSERT INTO " . Dealer::$CLASS_TABLE . " (" . implode(",", array_keys($fields)) . ") VALUES (" . implode(',', $insert["question_marks"]) . ")";

		$pdo = $d->getPDO();
		$query = $pdo->prepare($sql);
		$query->execute($insert["data"]);
		$insert_id = $pdo->lastInsertId();

		return $insert_id;
	}

	public static function delete($data) {
		$d = new Dealer();
		$sql = 'DELETE FROM ' . Dealer::$CLASS_TABLE . ' WHERE dealer_id = :dealer_id';
		$fields = array();
		$fields['dealer_id'] = $data['dealer_id'];

		$pdo = $d->getPDO();
		$query = $pdo->prepare($sql);
		$query->execute($fields);
		$insert_id = $pdo->lastInsertId();
		if($insert_id) {
			return true;
		} else {
			return 'there was an error deleting the dealer';
		}
	}

	static function update_from_post($post) {
		
		$fields = array();

		// handle image first then process query
		$keys = new Dealer();
		foreach ($keys as $key => $val) {
			if (isset($post[$key])) $fields[$key] = $keys->sanitizeValue($post[$key]);
		}

		if(isset($post["counties"])){
			$fields["counties"] = (is_array($post["counties"])) ? json_encode($post["counties"]) : json_encode(array());
		}
		
		$fields["state"] = (!empty($post["state-ca"]) && empty($post["state"])) ? $post["state-ca"] : $fields["state"];

		$fields['headquarters_flag'] = ($fields['headquarters_flag'] == 1) ? 1 : 0; //set flag accordingly
		$fields['updated_at'] = date("Y-m-d H:i:s"); // set updated timestamp
		
		//update lat and lng for user
		if(!empty($post['address']) || !empty($post['city']) || !empty($post['state']) || !empty($post['zip_code'])) {
			$address = $post['address'] . "," . $post['city'] . ", " . $post['state'] . " " . $post['zip_code'];
			$result = Dealer::googleGet($address);
			$fields['longitude'] = $result['longitude'];
			$fields['latitude'] = $result['latitude'];
		}

		$where_field = "dealer_id";

		$table = "dealers";

		$update = $keys->prepareUpdate($fields, $keys);

		//build the where statement
		$where = $keys->prepareWhere($fields, $where_field, $where_field);
		//combine values for the db statement
		if($where["statement"])
			$update["values"] = array_merge($update["values"], $where["values"]);

		// putting them together to form the update statement			
		$sql = "UPDATE $table SET " . $update["set"] . $where["statement"];
		
		$pdo = $keys->getPDO();
		$stmt = $pdo->prepare($sql);
		$update = $stmt->execute($update["values"]);

		return $update;
	}

	//google get
	public function googleGet($googleAddress) {
		$callURL = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode( $googleAddress ) .'&sensor=false');
		
		$json_result = json_decode($callURL);
		
		$tmpResults =  $json_result->results[0];
		$coordinates = $tmpResults->geometry->location;
		$latcoord = $coordinates->lat;
  		$lngcoord = $coordinates->lng;
  		if(!empty($lngcoord) && !empty($latcoord)) {
			//set results longitude and latitude
			$data['longitude'] = $lngcoord;
			$data['latitude'] = $latcoord;
		} else {
			$data['longitude'] = null;
			$data['latitude'] = null;
		}

		return $data;
	}

	//get lat and lon from table zip_code
	public function getLatLng($data) {
		
		// $sql = "SELECT * FROM geo_zip_code WHERE zip_code = '$data'";
		// $pdo = $this->getPDO();
		// $stmt = $pdo->prepare($sql);
		// $result = $stmt->execute();

		// if ($stmt->rowCount() > 0) {
		// 	$results = $stmt->fetchAll(\PDO::FETCH_OBJ);
		// }

		$result = Dealer::googleGet($data);
		$data['longitude'] = $result['longitude'];
		$data['latitude'] = $result['latitude'];
		
		return (object) $result;
	}

	//get lat and lon from table zip_code
	public function getCounty($data) {
		
		$sql = "SELECT * FROM geo_zip_code WHERE zip_code = '$data'";
		$pdo = $this->getPDO();
		$stmt = $pdo->prepare($sql);
		$result = $stmt->execute();

		if ($stmt->rowCount() > 0) {
			$results = $stmt->fetchAll(\PDO::FETCH_OBJ);
		}
		
		return $results[0];
	}

	//search for company
	public function search($data) {
		
		$util = new Util();
		$results = null;
		//getting lat & lng from zip_code table
		$dealers = array();
		$tmp = array();

		if(isset($data["zip_code"]) && !empty($data["zip_code"])){
			$current = $this->getCounty($data["zip_code"]);
			//print_r($current);
			if(!empty($current)){
				//print_r($range);
				$tmp = array();
				$table = "dealers";
				$sql = "SELECT * FROM " . $table;
				$sql .= " WHERE (headquarters_flag = 1) OR (state = '" . $current->state . "' AND counties LIKE  '%" . $current->county . "%') ORDER BY headquarters_flag ASC";		
				//echo $sql;
				$objs = array();

				$pdo = $this->getPDO();
				$stmt = $pdo->prepare($sql);
				$result = $stmt->execute();
				if ($stmt->rowCount() > 0) {
					$result = $stmt->fetchAll(\PDO::FETCH_CLASS);
					foreach ($result as $row) {
						if($row->headquarters_flag == 0 ){
							//$row->distance = $util->distanceInFeet($current->latitude, $current->longitude, $row->latitude, $row->longitude);
							foreach ($row as $key => $val) {
								if(in_array($key, Dealer::$jsons)){
									$row->$key = json_decode($val);
								} else {
									$row->$key = $val;
								}
							}
							foreach(Dealer::$remove as $x){
								unset($row->$x);
							}

							if($row->distance["distance"] <= $data["range"]){
						  		$tmp[$row->dealer_id] = $row;
						  	}
						  } else {
						  	$headquarters[] = $row;
						  }
					}
				}
			}
		}

		$results["retailers"] = (!empty($tmp)) ? $tmp : null ;
		$results["headquarters"] = (!empty($headquarters)) ? $headquarters : null ;
		$results["range"] = $data["range"];

		return $results;
	}

	//search for company
	public function searchZip($data) {
		
		$util = new Util();
		$results = null;
		//getting lat & lng from zip_code table
		$dealers = array();
		$tmp = array();

		if(isset($data["zip_code"]) && !empty($data["zip_code"])){
			$current = $this->googleGet($data["zip_code"]);
			//print_r($current);
			if(!empty($current)){
				$data["range"] = (isset($data["range"]) && !empty($data["range"])) ? $data["range"] : 50 ;

				$range = $util->latLngRange($current->latitude, $current->longitude, $data["range"]);
				//print_r($range);
				$tmp = array();
				$table = "dealers";
				$sql = "SELECT * FROM " . $table;
				$sql .= " WHERE (headquarters_flag = 1) OR ((longitude > " . $range['lng_min'] . " AND longitude < " . $range['lng_max'] . ") AND ( latitude < " . $range['lat_min'] . " AND latitude > " . $range['lat_max'] . ")) ORDER BY headquarters_flag ASC";		

				$where_fields = array("city", "state");
				$where_tmp = array();
				$objs = array();

				$pdo = $this->getPDO();
				$stmt = $pdo->prepare($sql);
				$result = $stmt->execute();
				if ($stmt->rowCount() > 0) {
					$result = $stmt->fetchAll(\PDO::FETCH_CLASS);
					foreach ($result as $row) {
						if($row->headquarters_flag == 0 ){
							$row->distance = $util->distanceInFeet($current->latitude, $current->longitude, $row->latitude, $row->longitude);
							foreach ($row as $key => $val) {
								if(in_array($key, Dealer::$jsons)){
									$row->$key = json_decode($val);
								} else {
									$row->$key = $val;
								}
							}
							foreach(Dealer::$remove as $x){
								unset($row->$x);
							}

							if($row->distance["distance"] <= $data["range"]){
						  		$tmp[$row->dealer_id] = $row;
						  	}
						  } else {
						  	$headquarters[] = $row;
						  }
					}
				}

				if(!empty($tmp)){
					$distance_map = array();
					$sorted = array();
					foreach($tmp as $t){
						$distance_map[$t->dealer_id] = $t->distance["distance"];
					}
					asort($distance_map);
					foreach($distance_map as $k => $v){
						$sorted[$k] = $tmp[$k];
					}

					if(!empty($sorted)){
						foreach($sorted as $s){
							$dealers[] = $s;
						}
					}
				}
			}
		}

		$results["retailers"] = (!empty($dealers)) ? $dealers : null ;
		$results["headquarters"] = (!empty($headquarters)) ? $headquarters : null ;
		$results["range"] = $data["range"];

		return $results;
	}


	static function getByEmptyGeo() {
		global $modx;
		
		$table = Dealer::$CLASS_TABLE;
		//$result = $modx->select('id, address, city, state, zip',$table,'longitude = NULL','',5);
		$sql = "SELECT dealer_id, address, city, state, zip_code FROM $table WHERE (longitude IS NULL OR longitude = '')  LIMIT 500";
		//echo $sql;
		$result = $modx->query($sql);
		while($row = $result->fetch(PDO::FETCH_OBJ)){
		//print_r($row);
			$data['dealer_id'] = $row->dealer_id;
			$data['address'] = $row->address;
			$data['city'] = $row->city;
			$data['state'] = $row->state;
			$data['zip_code'] = $row->zip_code;
			//Dealer::pts($data);
			Dealer::update_geo($data);
		}

	}

	static function update_geo($data) {
		global $modx;
		$fields = array();

		// handle image first then process query
		$keys = new Dealer();
		foreach ($keys as $key => $val) {
			if (isset($data[$key])) $data[$key] = $keys->sanitizeValue($data[$key]);
		}
		
		$data['updated_at'] = date("Y-m-d H:i:s"); // set updated timestamp
		
		//update lat and lng for user
		if(!empty($data['address']) || !empty($data['city']) || !empty($data['state']) || !empty($data['zip_code'])) {
			$address = $data['address'] . "," . $data['city'] . ", " . $data['state'] . " " . $data['zip_code'];
			$result = Dealer::googleGet($address);
			$data['longitude'] = $result['longitude'];
			$data['latitude'] = $result['latitude'];
			//Dealer::pts($data);
		}

		$where_field = "dealer_id";

		$table = "dealers";

		$update = $keys->prepareUpdate($data, $keys);

		//build the where statement
		$where = $keys->prepareWhere($data, $where_field, $where_field);
		// $this->pts($update);
		// die();
		//combine values for the db statement
		if($where["statement"])
			$update["values"] = array_merge($update["values"], $where["values"]);

		// putting them together to form the update statement			
		$sql = "UPDATE $table SET " . $update["set"] . $where["statement"];
	//	echo $sql;

		Dealer::pts($update["values"]);
		
		$query = $modx->prepare($sql);
		$query->execute($update["values"]);

		return true;
	}

}
?>
