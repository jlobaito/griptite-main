<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
//echo " hi ";
if(!isset($modx))
{
	//echo " modx config ";
    require_once $_SERVER['DOCUMENT_ROOT'].'/config.core.php';
    require_once MODX_CORE_PATH.'model/modx/modx.class.php';
    $modx = new modX();
    $modx->initialize('web');
  //  $modx->getService('error','error.modError');
}

include_once($_SERVER['DOCUMENT_ROOT'].'/core/components/dealers/models/Dealer.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/core/components/common/models/Util.php');
 
$u = new Util();
$component_url = "/manager/?a=index&namespace=Dealers";
$controller_action = (isset($_REQUEST["action"])) ? $_REQUEST["action"] : $controller_action ;
$dump = (isset($_REQUEST["dump"])) ? true : false ;
ob_start();
$output = "";
	switch($controller_action){

		case 'get-all':
			$dealers = Dealer::getAll();
			
			include "views/chunks/manager/list.php";
			
		break;

		case 'edit':
			$dealer = new Dealer($_GET["dealer_id"]);
			$counties = $u->getCounties();
			//$u->pts($dealer);
			// print_r($counties);
			include "views/chunks/manager/edit.php";
				

		break;
		
		case 'update':
			$response["status"] = false;
			$data = (isset($_POST["save"])) ? $_POST : $_GET;

			if(isset($data['save'])){
				if(Dealer::update_from_post($data)){
					header("Location: " . $component_url . "&action=edit&dealer_id=" . $data["dealer_id"]);
				};
			}
		break;

		case 'create':
			$response["status"] = false;
			$data = (isset($_POST["save"])) ? $_POST : $_GET;

			$data["dealer_id"]  = Dealer::create($data);
			header("Location: " . $component_url . "&action=edit&dealer_id=" . $data["dealer_id"]);

		break;

		case 'search' :
			$response['status'] = false;
			$data = (isset($_POST["action"])) ? $_POST : $_GET;
			if(isset($data['zip_code'])){
				$user = new Dealer();
				$results = $user->search($data);
				$response['data'] = $results;
				$response['status'] = true;
			} else {
				//zip code was not set
				$response['data'] = null;
				$response['status'] = true;
				$response["message"] = $u->alert[10001];
			}
		break;

		case 'setGeo' :
			echo "in SetGeo";
			$u = new dealer();
			Dealer::getByEmptyGeo();

		break;

		case 'remove-dealer':
			$response["status"] = false;
			$data = (isset($_POST["dealer_id"])) ? $_POST : $_GET;
			if(isset($data['dealer_id'])) {
				$removed = Dealer::delete($data);
				if($removed == true) {
					header("Location: " . $component_url . "&action=dealer_removed");
				} else {
					header("Location: " . $component_url . "&action=cannot_remove");
				}
				
			} else {
				header("Location: " . $component_url . "&action=cannot_remove");
			}
		break;

		default:

		$dealers = Dealer::getAll();

		// print_r($dealers);
		// die();
		
		include "views/chunks/manager/list.php";

		break;
}
$output = ob_get_contents();
ob_end_clean();
if($dump){
	echo $output;
} else {
	return $output;
}
//echo json_encode($response);
?>