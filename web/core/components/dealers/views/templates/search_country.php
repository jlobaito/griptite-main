<?php 

    if(!isset($modx)){
        require_once $_SERVER["DOCUMENT_ROOT"] . '/config.core.php';
        require_once MODX_CORE_PATH.'model/modx/modx.class.php';
        $modx = new modX();
        $modx->initialize('web');
        $modx->getService('error','error.modError');
    }

require_once($_SERVER["DOCUMENT_ROOT"] .'/core/components/dealers/models/Dealer.php');

$d = new Dealer();

$countries = $d->getCountries();

?>
<!DOCTYPE html>
<html lang="en" class="secondary">
    <head>
        <meta charset="utf-8">
        <base href="/" />
        <title>Find Dealers / Contacts</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="/resources/css/bootstrap.css" rel="stylesheet">
        <link href="/resources/js/datatables/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
        <script src="//code.jquery.com/jquery.js"></script>
        <script>
                //$j = jQuery.noConflict();
                var sto = {data:{},app:{}};
        </script>
    </head>
    <body>
        <div id="loading-row" style = "display: none;">
            <div id="loading-frame">
                <img src="/resources/img/loading.gif">
            </div>
        </div>
        <div class="container">
            <div class="row-fluid">
                <div class="span6">
                    <h3 class="search-heading">Search by Location</h3>
                    <form id="search-location-form">
                        <select class="span7 state" name="country">
                            <option value = "">Select Country</option>
                            <?
                            foreach($countries as $c){?>
                                <option value="<?= $c ?>"><?= $c ?></option>
                            <?}?>
                        </select>
                        <div>
                            <input class="span7" name="city" type="text" placeholder="City">
                            <input name="international_flag" type="hidden" value="1">
                        </div>
                        <button class="btn submit-search" type="button" data-loading-text="<i class='icon-refresh icon-spin'></i> Searching..." data-form = "#search-location-form">Search</button>
                    </form>
                </div>
                <div class="span1 divider hidden-phone">
                    <div class="or-line"></div>
                    <div class="or-txt">or</div>
                </div>
                <div class="span5">
                    <h3 class="search-heading">Search by Name</h3>
                    <form id="search-text-form">
                        <div>
                            <input class="" name="contact" type="text" placeholder="Name">
                        </div>
                        <div>
                            <input class="" name="company" type="text" placeholder="Company">
                        </div>
                        <div id="name-more-options-link-row">
                            <span data-target ="#text-more-options" class = "more-options"> More Options</span>
                        </div>
                        <div id="text-more-options" style = "display: none;">
                            <select class="span7 country" name="country">
                            </select>
                            <div>
                                <input class="span7" name="city" type="text" placeholder="City">
                            <input name="international_flag" type="hidden" value="1">
                            </div>
                        </div>
                        <button class="btn submit-search" type="button" data-loading-text="<i class='icon-refresh icon-spin'></i> Searching..." data-form = "#search-text-form">Search</button>
                    </form>
                </div>
            </div>
            <div class="row-fluid">
                <div class=" callout">
                    <label class="checkbox">
                        <input type="checkbox" class="retailers-only" > Show me only Retailers
                    </label>
                </div>
            </div>
            <div class="row-fluid search-results results-list-target">
            </div>
        </div>

        <div type = "text/html" class = "results-list-template hide">
        	<h1>Results</h1>
        	<div class = "span5 contact-list">
        		<h2>Contacts</h2>
				<ul>
					<% u.each(contacts, function(e,i,l){%>
						<li>
							<strong><%=  (e.company) ? e.company : e.contact %></strong><br />
							<%=  (e.company && e.contact) ? e.contact + '<br /> ' : '' %>
							<%=  (e.address) ? e.address + '<br />' : '' %>
							<%=  (e.address2) ? e.address2 + '<br />' : '' %>
							<%=  (e.city) ? e.city + ',' : '' %>
							<%=  (e.state) ? e.state + ' ' : '' %>
							<%=  (e.zip_code) ? e.zip_code + '<br />' : '' %>
							<%=  (e.territories) ? 'Terrotories: ' + e.territories + '<br />' : '' %>
							<%=  (e.phone) ?  'Phone: ' + e.phone + '<br />' : '' %>
							<%=  (e.tollfree) ?  'Tollfree: ' + e.tollfree + '<br />' : '' %>
							<%=  (e.email) ?  'Email: <a href = "mailto:' + e.email + '">' + e.email + '</a><br />' : '' %>
							<%=  (e.website) ?  'Website: <a href = "' + e.website + '">' + e.website + '</a><br />' : '' %>
						</li>
					<% }); %>
				</ul>
			</div>
			<div class = "span6 retailer-list">
        		<h2>Retailers</h2>
				<ul>
					<% u.each(retailers, function(e,i,l){%>
						<li>
							<strong><%=  (e.company) ? e.company : e.contact %></strong><br />
							<%=  (e.company && e.contact) ? e.contact + '<br /> ' : '' %>
							<%=  (e.address) ? e.address + '<br />' : '' %>
							<%=  (e.address2) ? e.address2 + '<br />' : '' %>
							<%=  (e.city) ? e.city + ',' : '' %>
							<%=  (e.state) ? e.state + ' ' : '' %>
							<%=  (e.zip_code) ? e.zip_code + '<br />' : '' %>
							<%=  (e.distance) ? 'Distance: ' + e.distance.distance + '<br />' : '' %>
							<%=  (e.territories) ? 'Terrotories: ' + e.territories + '<br />' : '' %>
							<%=  (e.phone) ?  'Phone: ' + e.phone + '<br />' : '' %>
							<%=  (e.tollfree) ?  'Tollfree: ' + e.tollfree + '<br />' : '' %>
							<%=  (e.email) ?  'Email: <a href = "mailto:' + e.email + '">' + e.email + '</a><br />' : '' %>
							<%=  (e.website) ?  'Website: <a href = "' + e.website + '">' + e.website + '</a><br />' : '' %>
						</li>
					<% }); %>
				</ul>
			</div>

		</div>
        <script src="/resources/js/bootstrap.min.js"></script>
        <script src="/resources/js/underscore.min.js"></script>
        <script>
        var u = _.noConflict();
        </script>
        <script src="/resources/js/datatables/js/jquery.dataTables.js"></script>
        <script src="/assets/components/common/js/sto.utils.js"></script>
        <script src="/assets/components/dealers/js/sto.search.js"></script>
    </body>
</html>