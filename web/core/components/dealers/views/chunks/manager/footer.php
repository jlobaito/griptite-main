<?php

?>
<script src="/resources/js/bootstrap.min.js"></script>
<script src="/resources/js/underscore.min.js"></script>
<script>
	var u = _.noConflict();
</script>
<script src="/resources/js/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/components/dealers/js/jlo.utils.js"></script>
<script src="/assets/components/dealers/js/jlo.dealers.js"></script>

<script>

	$(document).ready(function() {
		$('.remove-dealer').unbind('click').click(function(e) {
			e.preventDefault();
			var $e = $(this);
			var url = $e.data("url");
			var r = confirm("Confirm Removal!");
			if (r == true) {
				window.location = url;
			}
		});
	});

</script>