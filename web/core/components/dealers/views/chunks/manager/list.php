<?php
include "header.php";
?>
<div class = "container">
	<h1>Grip-Tite Dealers</h1>
	<a href = "<?= $component_url ?>&action=create" class = "btn">New Dealer</a>
	<div class = "">
		<div class="table-responsive">
			<table id = "dealertable" class="table table-bordered">
				<thead>
					<tr>
						<th>id</th>
						<th>company</th>
						<th>city</th>
						<th>state</th>
						<th>remove</th>
					</tr>
				</thead>
				<tbody>
					<?php
					if(!empty($dealers)){
						foreach($dealers as $d){
							include "table_row.php";
						}
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
include "footer.php";
?>

<script>
	$(document).ready(function() {
		$('#dealertable').dataTable();
	});
</script>