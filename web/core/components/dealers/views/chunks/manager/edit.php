<?php
	include "header.php";
?>

<script>
    jlo.data.counties = <?= json_encode($counties) ?>;
    jlo.data.active_counties = <?= json_encode($dealer->counties) ?>;
    jlo.data.active_state = "<?= $dealer->state ?>";
</script>
<a href="<?= $component_url ?>" class="btn">All Dealers</a>
<h3>Edit Dealer <?= $dealer->company ?></h3>
<button data-url="<?= $component_url ?>&action=remove-dealer&dealer_id=<?= $dealer->dealer_id ?>" type="button" class="btn btn-small btn-danger remove-dealer">Remove Dealer</button>
<h4>Dealer Details</h4>
<form action="<?= $component_url ?>" method="post" class="form-horizontal" role="form" >
	
    <div class="control-group">
        <label class="control-label" for="headquarters_flag">Headquarters: </label>
        <div class="controls">
            <input type="checkbox" class="headquarters_flag" name="headquarters_flag" value="1" <?= ($dealer->headquarters_flag == 1) ? "checked" : "" ?> style="margin-top:10px"/>
        </div>
    </div>

	<div class="control-group">
        <label class="control-label" for="company">Company: </label>
        <div class="controls">
            <input type="text" id="company" placeholder="Enter Company" name="company" value="<?= $dealer->company ?>">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="content">Content line: </label>
        <div class="controls">
            <textarea id="content" placeholder="Call Jason..." name="content"><?= $dealer->content ?></textarea>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="email">Email Address: </label>
        <div class="controls">
            <input type="email" id="email" placeholder="Enter Email" name="email" value="<?= $dealer->email ?>">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="phone">Phone Number: </label>
        <div class="controls">
            <input type="text" id="phone" placeholder="Enter Phone" name="phone" value="<?= $dealer->phone ?>">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="tollfree">Toll Free: </label>
        <div class="controls">
            <input type="text" id="tollfree" placeholder="Enter Toll Free" name="tollfree" value="<?= $dealer->tollfree ?>">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="fax">Fax: </label>
        <div class="controls">
            <input type="text" id="fax" placeholder="Enter Fax" name="fax" value="<?= $dealer->fax ?>">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="website">Website: </label>
        <div class="controls">
            <input type="text" id="website" placeholder="Enter Website" name="website" value="<?= $dealer->website ?>">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="address">Address: </label>
        <div class="controls">
            <input type="text" id="address" placeholder="Enter Address" name="address" value="<?= $dealer->address ?>">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="address2">Address 2: </label>
        <div class="controls">
            <input type="text" id="address2" placeholder="Enter Address 2" name="address2" value="<?= $dealer->address2 ?>">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="city">City: </label>
        <div class="controls">
            <input type="text" id="city" placeholder="Enter City" name="city" value="<?= $dealer->city ?>">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="zip_code">Postal Code: </label>
        <div class="controls">
            <input type="text" id="zip_code" placeholder="Enter Postal Code" name="zip_code" value="<?= $dealer->zip_code ?>">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="country">Country: </label>
        <div class="controls">
            <select id="country" name="country" class = "dealer-country">
                <option value = "">Choose Country</option>
                <option value = "CA" <?= ($dealer->country == 'CA') ? 'selected' : '' ?>>Canada</option>
                <option value = "US" <?= ($dealer->country == 'US') ? 'selected' : '' ?>>United States</option>
            </select>
        </div>
    </div>

	<div class="control-group">
        <div class="hidden-ca <?= ($dealer->country == 'CA') ? 'hide' : '' ?>">
            <label class="control-label" for="state">State: </label>
            <div class="controls">
                <?= $u->stateSelect('abbr','state','state','form-control dealer-state',$dealer->state ) ?>
            </div>
        </div>

    	<div class="hidden-us <?= ($dealer->country == 'US') ? 'hide' : '' ?>">
			<label class="control-label" for="state">State: </label>
			<div class="controls">
            <select id="state-ca" name="state-ca" class = "state-select">
                <option value = "">Choose Region</option>
                <option value="AB" <?= ($dealer->state == 'AB') ? 'selected' : '' ?>>Alberta</option>
                <option value="BC" <?= ($dealer->state == 'BC') ? 'selected' : '' ?>>British Columbia</option>
                <option value="MB" <?= ($dealer->state == 'MB') ? 'selected' : '' ?>>Manitoba</option>
                <option value="NB" <?= ($dealer->state == 'NB') ? 'selected' : '' ?>>New Brunswick</option>
                <option value="NL" <?= ($dealer->state == 'NL') ? 'selected' : '' ?>>Newfoundland and Labrador</option>
                <option value="NS" <?= ($dealer->state == 'NS') ? 'selected' : '' ?>>Nova Scotia</option>
                <option value="ON" <?= ($dealer->state == 'ON') ? 'selected' : '' ?>>Ontario</option>
                <option value="PE" <?= ($dealer->state == 'PE') ? 'selected' : '' ?>>Prince Edward Island</option>
                <option value="QC" <?= ($dealer->state == 'QC') ? 'selected' : '' ?>>Quebec</option>
                <option value="SK" <?= ($dealer->state == 'SK') ? 'selected' : '' ?>>Saskatchewan</option>
                <option value="NT" <?= ($dealer->state == 'NT') ? 'selected' : '' ?>>Northwest Territories</option>
                <option value="NU" <?= ($dealer->state == 'NU') ? 'selected' : '' ?>>Nunavut</option>
                <option value="YT" <?= ($dealer->state == 'YT') ? 'selected' : '' ?>>Yukon</option>
            </select>
			</div>
		</div>
	</div>

    <div class="control-group hidden-ca">
        <div class="county-list <?= ($dealer->country == 'US') ? '' : 'hide' ?>">
            <label class="control-label" for="counties">Counties: </label>
            <div class="span10 county-list-target">
                <?  
                if(!empty($counties[$dealer->state])){
                    foreach($counties[$dealer->state] as $c){?>
                    <label class="checkbox-inline span2">
                        <input type="checkbox" name="counties[]" value="<?= $c ?>" <?= (in_array($c, $dealer->counties)) ? "checked" : "" ?>> <?= $c ?>
                    </label>
                    <?}
                }
                ?>
            </div>
        </div>
    </div>
    
	<div class="control-group">
    	<div class="controls">
			<input type="hidden" name="action" value="update" />
			<input type="hidden" name="dealer_id" value="<?= $dealer->dealer_id ?>" />
			<input type="hidden" name="save" value="true" />
			<button type="submit" class="btn btn-primary">Submit</button> 
			<a class="btn btn-danger cancel-submit" href="<?= $component_url ?>">Cancel</a>
		</div>
	</div>
</form>

<script type="text/html" class="county-list-template hide" >
    <% u.each(counties, function(e,i,l){
        //jlo.util.log('((' + jlo.data.active_state + ' == ' + jlo.data.current_state + ') && ' + $.inArray(e, jlo.data.active_counties) + ' > -1)');
        var checked =  ((jlo.data.active_state == jlo.data.current_state) && $.inArray(e, jlo.data.active_counties) > -1) ? "checked" : "" ;%>
        <label class="checkbox-inline span2">
            <input type="checkbox" name="counties[]" value="<%= e %>" class="<%= checked%>"> <%= e %>
        </label>
    <% }); %>
</script>


<?php
	include "footer.php";
?>